*Settings*
Documentation                 Validar login da área do dashboard ZAK.

*Variables*
${groupName}                  flavias
${username}                   flavia
${password}                   flavia123

*Keywords*
Inserir credenciais de acesso e visualizar dashboard ZAK

    Fill text                 id=groupName         ${groupName}       
    Fill text                 id=username          ${username}
    Fill text                 id=password          ${password}
    
    Click                     id=submitButton

#Click para abrir relatórios    
    Click                     text=Monday Restaurant
    Click                     text=Thuesday`s Bar

#Sleep usado para que o elemento de relatório tenha sua opacidade visível em 100%
    Sleep                     2

Inserir credenciais de acesso e visualizar a mensagem de erro

    Fill text                 id=groupName           ${groupName}        
    Fill text                 id=username            qualiti
    Fill text                 id=password            qa123
    
    Click                     id=submitButton

    Get Text                  css=span               contains                   Login recusado

Inserir credencial e visualizar a mensagem de erro de grupo

    Fill text                 id=groupName           Ráscal        
    Fill text                 id=username            ${username}
    Fill text                 id=password            ${password}
    
    Click                     id=submitButton

    Get Text                  css=span               contains                   Grupo não encontrado

Validar sessão de esqueci senha

    Click                     id=forgotPassword

    Fill Text                 css=input[placeholder=Grupo]                      flavia 
    Fill Text                 css=input[placeholder=E-mail]                     fvastres@zak.app

    Click                     text=Recuperar Senha